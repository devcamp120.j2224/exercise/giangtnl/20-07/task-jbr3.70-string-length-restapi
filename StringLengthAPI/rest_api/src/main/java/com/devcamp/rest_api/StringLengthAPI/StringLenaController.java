package com.devcamp.rest_api.StringLengthAPI;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class StringLenaController {
    @GetMapping(value = "/length")
    public int stringleng(@RequestParam(value = "string",defaultValue = "1")String string) {
        int length;
        // String[] strArray = string.split("\\s");
        length = string.length();
        return length;
    }
}
